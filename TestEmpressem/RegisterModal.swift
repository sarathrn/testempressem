//
//  RegisterModal.swift
//  TestEmpressem
//
//  Created by apple on 08/02/17.
//  Copyright © 2017 Sarath Raveendran. All rights reserved.
//

import Foundation

class RegisterModal {
    static var sharedInstance = RegisterModal()
    
    var message: String?
    var statusCode: String?
    var user = User()
}

class User {
    
    var email: String?
    var id: String?
    var name: String?
    var token: String?
    var userName: String?
}
