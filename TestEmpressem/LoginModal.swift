//
//  User.swift
//  TestEmpressem
//
//  Created by apple on 08/02/17.
//  Copyright © 2017 Sarath Raveendran. All rights reserved.
//

import Foundation

class LoginModal: NSObject, NSCoding {
    static var sharedInstance = LoginModal()
    
    var message: String?
    var statusCode: String?
    var user = LoginUser()
    
    // Transformable Modal Fetch & Retrival DB
    private static let key_message = "message"
    private static let key_statusCode = "statusCode"
    private static let key_user = "user"
    
    override init() {
    }
    
    required init(coder aDecoder: NSCoder) {
        message = aDecoder.decodeObject(forKey:LoginModal.key_message) as? String
        statusCode = aDecoder.decodeObject(forKey:LoginModal.key_statusCode) as? String
        if let _user = aDecoder.decodeObject(forKey:LoginModal.key_user) as? LoginUser {
            user = _user
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(message, forKey: LoginModal.key_message)
        aCoder.encode(statusCode, forKey: LoginModal.key_statusCode)
        aCoder.encode(user, forKey: LoginModal.key_user)
    }

    
}


class LoginUser: NSObject, NSCoding {
   
    var id: String?
    var name: String?
    var email: String?
    var userName: String?
    var token: String?
    var profilePic = ProfilePic()
    
    // Transformable Modal Fetch & Retrival DB
    private static let key_id = "id"
    private static let key_name = "name"
    private static let key_email = "email"
    private static let key_userName = "userName"
    private static let key_token = "token"
    private static let key_profilePic = "profilePic"
    
    override init() {
    }
    
    required init(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey:LoginUser.key_id) as? String
        name = aDecoder.decodeObject(forKey:LoginUser.key_name) as? String
        email = aDecoder.decodeObject(forKey:LoginUser.key_email) as? String
        userName = aDecoder.decodeObject(forKey:LoginUser.key_userName) as? String
        token = aDecoder.decodeObject(forKey:LoginUser.key_token) as? String
        if let _profilePic = aDecoder.decodeObject(forKey:LoginUser.key_profilePic) as? ProfilePic {
            profilePic = _profilePic
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: LoginUser.key_id)
        aCoder.encode(name, forKey: LoginUser.key_name)
        aCoder.encode(email, forKey: LoginUser.key_email)
        aCoder.encode(userName, forKey: LoginUser.key_userName)
        aCoder.encode(token, forKey: LoginUser.key_token)
        aCoder.encode(profilePic, forKey: LoginUser.key_profilePic)

    }

}

class ProfilePic: NSObject, NSCoding {
    var url: String?
    var thumb = Thumb()
    
    // Transformable Modal Fetch & Retrival DB
    private static let key_url = "url"
    private static let key_thumb = "thumb"
    
    override init() {
    }
    
    required init(coder aDecoder: NSCoder) {
        url = aDecoder.decodeObject(forKey:ProfilePic.key_url) as? String
        if let _thumb = aDecoder.decodeObject(forKey:ProfilePic.key_thumb) as? Thumb {
            thumb = _thumb
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(url, forKey: ProfilePic.key_url)
        aCoder.encode(thumb, forKey: ProfilePic.key_thumb)
    }

}

class Thumb: NSObject, NSCoding {
    var url: String?
    
    // Transformable Modal Fetch & Retrival DB
    private static let key_url = "url"
    
    override init() {
    }
    
    required init(coder aDecoder: NSCoder) {
        url = aDecoder.decodeObject(forKey:Thumb.key_url) as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(url, forKey: Thumb.key_url)
    }

}







