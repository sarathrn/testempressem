//
//  ProfileTableViewController.swift
//  TestEmpressem
//
//  Created by apple on 09/02/17.
//  Copyright © 2017 Sarath Raveendran. All rights reserved.
//

import UIKit

class ProfileTableViewController: UITableViewController {

    // MARK: Connection Object
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    
    // Declarations
    let userModal = LoginModal.sharedInstance
    let urls = APIUrls.sharedInstance
    
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: Arrange View
    func configureView() {
     
    }
    
    
    // MARK: Events
    func setData() {
        
        clear()
        
        if let profileImageUrlString = userModal.user.profilePic.url, let url = URL(string: "\(urls.primaryUrl)\(profileImageUrlString)") {
            profileImageView.setImageWith(url, placeholderImage: UIImage(named: "profile_dummy"), options: SDWebImageOptions.refreshCached, usingActivityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        }
       
        if let name = userModal.user.name {
            nameLabel.text = name
        }
        
        if let userName = userModal.user.userName {
            userNameLabel.text = userName
        }
        if let email = userModal.user.email {
            emailLabel.text = email
        }
        
    }
    
    // Reset values
    func clear() {
        nameLabel.text = nil
        userNameLabel.text = nil
        emailLabel.text = nil
        profileImageView.image = UIImage(named: "profile_dummy.jpg")
    }

    
    // MARK: TableView DataSources
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
}
