//
//  RegisterParser.swift
//  TestEmpressem
//
//  Created by apple on 08/02/17.
//  Copyright © 2017 Sarath Raveendran. All rights reserved.
//

import Foundation

class RegisterParser {
    
    // Declaration
    let keys = APIKeys.sharedInstance
    
    // Parse
    func parse(response: Dictionary<String, Any>, modal: RegisterModal, completionHandler: () -> ()) {
        
        if let statusCode = response[keys.statuscode] as? String {
            modal.statusCode = statusCode
        }
        if let message = response[keys.message] as? String {
            modal.message = message
        }
        
        /* Division Found*/
        if let user = response[keys.user] as? Dictionary<String, Any> {
            
            if let email = user[keys.email] as? String {
                modal.user.email = email
            }
            if let id = user[keys.id] {
                modal.user.id = "\(id)"
            }
            if let name = user[keys.name] as? String {
                modal.user.name = name
            }
            if let token = user[keys.token] as? String {
                modal.user.token = token
            }
            if let userName = user[keys.username] as? String {
                modal.user.userName = userName
            }
            
        }
        
        // Delegation
        completionHandler()
    }
    
}
