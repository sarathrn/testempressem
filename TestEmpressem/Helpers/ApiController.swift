//
//  ApiController.swift
//  TestEmpressem
//
//  Created by apple on 08/02/17.
//  Copyright © 2017 Sarath Raveendran. All rights reserved.
//

import Foundation

// APi Connection Token
class APIToken {
    static let sharedInstance = APIToken()
    
    let token = "ABCD"
}

// Support API Status comparison
enum APIStatus: String {
    case success = "200"
}

// Api Connection Urls
class APIUrls {
    static let sharedInstance = APIUrls()
    
    let primaryUrl = "http://recordio.empressemdemo.com/"
    let register = "register/signupapi"
    let login = "register/login"
}


// Get Or Post Keys
class GetPostKeys {
    static let sharedInstance = GetPostKeys()
    
    let name = "name"
    let username = "username"
    let email = "email"
    let password = "password"
    let token = "token"
}

// API keys
class APIKeys {
    static let sharedInstance = APIKeys()
    
    let message = "message"
    let statuscode = "statuscode"
    let user = "user"
    let email = "email"
    let id = "id"
    let name = "name"
    let token = "token"
    let username = "username"
    let profilepic = "profilepic"
    let url = "url"
    let thumb = "thumb"
}









