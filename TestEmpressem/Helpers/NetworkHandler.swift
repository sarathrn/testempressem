//
//  NetworkHandler.swift
//  TestEmpressem
//
//  Created by apple on 08/02/17.
//  Copyright © 2017 Sarath Raveendran. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
import Alamofire

class NetworkHandler {
    static let sharedInstance = NetworkHandler()
    
    // Declaration
    let urls = APIUrls.sharedInstance
    
   
    /*   POST Request */
    func post(url: String, params: Dictionary<String, Any>, showLoader: Bool, vc: UIViewController, completionHandler: @escaping (_ networkStatus: Bool, _ response: Any?) -> ()) {
        
        if !connectedToNetwork() {
            if logActivity { print("\nNetworkHandler: POST: No internect connection\n") }
            completionHandler(false, nil)
            return
        }
        
        if showLoader {
            vc.showIndicator()
        }
        
        
        Alamofire.request(makeUrl(url: url), method: .post, parameters: params, encoding: URLEncoding.default).validate().responseJSON { response in
            
            if showLoader {
                vc.hideIndicator()
            }
            
            switch response.result {
            case .success:
                if logActivity {
                    guard let respone = response.result.value else {
                        if logActivity {
                            print("NetworkHandler reporting -> Error on result parsing")
                        }
                        completionHandler(true, nil)
                        return
                    }
                    
                    completionHandler(true, respone)
                    return
                }
                
                
            case .failure(let error):
                completionHandler(true, nil)
                if logActivity {
                    print(response.request ?? "Nothing")  // original URL request
                    print(response.response ?? "Nothing") // HTTP URL response
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "Nothing") // server data
                    print(response.result)   
                    print(error)
                }
                
            }
            
        }
        
    }
    
    
    // crate url to network call
    func makeUrl(url: String) -> String {
        
        return urls.primaryUrl + url
        
    }

    
    /*          ----         -----           ----                ----         */
    // Connectivity
    func connectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
        
    
}
