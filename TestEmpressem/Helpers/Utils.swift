//
//  Utils.swift
//  TestEmpressem
//
//  Created by apple on 08/02/17.
//  Copyright © 2017 Sarath Raveendran. All rights reserved.
//

import Foundation
import UIKit

// Log Your Activity
let logActivity = true


class Colors {
    static let sharedInstance = Colors()
    
    let themeColor = UIColor(hexString: "#3278b3")
    let loginPageBackground = UIColor(hexString: "#3278b3")
    let registerPageBackground = UIColor(hexString: "#3278b3")
    
}


class WarnningMessages {
    static let sharedInstance = WarnningMessages()
    
    let noConnection = "No Internet Connection!"
    let invalidEmail = "Invalid Email"
    let invalidPassword = "Invalid Password"
    let invalidName = "Invalid Name"
    let invalidUserName = "Invalid Name"
    let invalidToken = "Invalid Token"
}
