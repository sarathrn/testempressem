//
//  CameraViewController.swift
//  TestEmpressem
//
//  Created by apple on 09/02/17.
//  Copyright © 2017 Sarath Raveendran. All rights reserved.
//

import UIKit
import Photos

class CameraViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    // MARK: Declarations
    var currentImage: UIImage?
    var currentVedioPath: URL?
    
    // MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Arrange View
    func configureView() {
        
        // Set Camera Option
        let cameraImageView = UIImageView()
        cameraImageView.contentMode = .scaleAspectFit
        cameraImageView.image = UIImage(named: "camera_big")
        cameraImageView.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        cameraImageView.center = view.center
        view.addSubview(cameraImageView)
        // Add Camera Action
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(CameraViewController.getCamera))
        cameraImageView.addGestureRecognizer(tapGesture)
        cameraImageView.isUserInteractionEnabled = true
    }

    // Awake Camera
    func getCamera() {
    
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .camera
        imagePickerController.mediaTypes = UIImagePickerController.availableMediaTypes(for: UIImagePickerControllerSourceType.camera)!
        imagePickerController.videoMaximumDuration = 30
        imagePickerController.delegate = self
        DispatchQueue.main.async {
            self.present(imagePickerController, animated: true, completion: nil)
        }
        
    }
    
    func awakeFromMenu() {
        if isViewLoaded {
            getCamera()
        }
    }
    
    //  MARK: UIImagePicker Delegates
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            currentImage = pickedImage
            currentVedioPath = nil
            saveWithUserPrompt()
        }
        else if let vedio = info[UIImagePickerControllerMediaURL] {
            currentVedioPath = vedio as? URL
            currentImage = nil
            saveWithUserPrompt()
        }
        else {
            currentImage = nil
            currentVedioPath = nil
        }
        
        dismiss(animated: true, completion: nil)
    
    }
    
    
    
    // Prompt Permission
    func saveWithUserPrompt() {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        if let image = currentImage {
            let save = UIAlertAction(title: "Save Image to PhotoAlbum", style: UIAlertActionStyle.default) { (UIAlertAction) in
                
                // Save to Album
                PHPhotoLibrary.shared().performChanges({ 
                    PHAssetChangeRequest.creationRequestForAsset(from: image)
                },
                completionHandler: { (status, error) in
                
                    if status {
                        DispatchQueue.main.async {
                            self.showMessage(message: "Image Saved Successfully")
                        }
                    }
                    
                })
                
            }
            actionSheet.addAction(save)
            
        }
        else if let vedioPath = currentVedioPath {
            
            let save = UIAlertAction(title: "Save Vedio Album", style: UIAlertActionStyle.default) { (UIAlertAction) in
            
                // Save to Album
                PHPhotoLibrary.shared().performChanges({
                
                    PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: vedioPath)
                },
                completionHandler: { (status, error) in
                
                    if status {
                        DispatchQueue.main.async {
                            self.showMessage(message: "Vedio Saved Successfully")
                        }
                    }
                    
                })
                
            }
            
            actionSheet.addAction(save)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil)
        actionSheet.addAction(cancel)
        
        DispatchQueue.main.async {
            self.present(actionSheet, animated: true, completion: nil)
        }
        
    }
    
    
}
