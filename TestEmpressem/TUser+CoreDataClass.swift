//
//  TUser+CoreDataClass.swift
//  TestEmpressem
//
//  Created by apple on 09/02/17.
//  Copyright © 2017 Sarath Raveendran. All rights reserved.
//

import Foundation
import CoreData


public class TUser: NSManagedObject, CDHelperEntity {
    public static var entityName: String! { return "TUser" }
}
