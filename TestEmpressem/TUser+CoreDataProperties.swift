//
//  TUser+CoreDataProperties.swift
//  TestEmpressem
//
//  Created by apple on 09/02/17.
//  Copyright © 2017 Sarath Raveendran. All rights reserved.
//

import Foundation
import CoreData


extension TUser {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TUser> {
        return NSFetchRequest<TUser>(entityName: "TUser");
    }

    @NSManaged public var data: Any?

}
