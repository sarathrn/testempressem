//
//  LoginParser.swift
//  TestEmpressem
//
//  Created by apple on 09/02/17.
//  Copyright © 2017 Sarath Raveendran. All rights reserved.
//

import Foundation

class LoginParser {
    
    // Declaration
    let keys = APIKeys.sharedInstance
    
    
    // Parse
    func parse(response: Dictionary<String, Any>, modal: LoginModal, completionHandler: () -> ()) {
        
        if let statusCode = response[keys.statuscode] as? String {
            modal.statusCode = statusCode
        }
        if let message = response[keys.message] as? String {
            modal.message = message
        }
        
        
        /* Division Found*/
        if let user = response[keys.user] as? Dictionary<String, Any> {
            
            if let email = user[keys.email] as? String {
                modal.user.email = email
            }
            if let id = user[keys.id] {
                modal.user.id = "\(id)"
            }
            if let name = user[keys.name] as? String {
                modal.user.name = name
            }
            if let token = user[keys.token] as? String {
                modal.user.token = token
            }
            if let userName = user[keys.username] as? String {
                modal.user.userName = userName
            }
            
            
            /* Division Found*/
            if let profilePic = (user[keys.profilepic] as? Dictionary<String, Any>)?[keys.profilepic] as? Dictionary<String, Any> {
                
                if let url = profilePic[keys.url] as? String {
                    modal.user.profilePic.url = url
                }
                
            
                /* Division Found*/
                if let thumb = profilePic[keys.thumb] as? Dictionary<String, Any> {
                    
                    if let url = thumb[keys.url] as? String {
                        modal.user.profilePic.thumb.url = url
                    }
                }
            
            }
            
        }
        
        // Delegation
        completionHandler()
    }
    
    
    func saveToDataBase(modal: LoginModal) {
        
        clearTable()
        
        let user = TUser.new()
        user.data = [modal]
        user.save()
    }
    
    func clearTable() {
        
        let user: [TUser] = TUser.findAll()
        for _user in user {
            _user.destroy()
        }
    }
    
    func fetchFromDataBase() {
        
        let user: [TUser] = TUser.findAll()
        if user.count > 0 {
            if let modal = (user[0].data as? Array<Any>)?[0] as? LoginModal {
                // ReSet & Set modal
                LoginModal.sharedInstance = LoginModal()
                LoginModal.sharedInstance = modal
            }
            else {
                // ResetModal
                LoginModal.sharedInstance = LoginModal()
                if logActivity { print("LoginParser -> fetchFromDataBase: No data found on data base to initilize the modal class") }
            }
        }
        
    }
    
}
