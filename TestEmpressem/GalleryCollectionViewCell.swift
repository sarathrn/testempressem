//
//  GalleryCollectionViewCell.swift
//  TestEmpressem
//
//  Created by apple on 09/02/17.
//  Copyright © 2017 Sarath Raveendran. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
 
    // MARK: Connection Objects
    @IBOutlet weak var thumbImageView: UIImageView!
    
    // Set Image
    var image: UIImage? {
        didSet {
            if let _image = image {
                self.thumbImageView.image = _image
            }
        }
    }
    
    // MARK: View Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()
    }
    
    // MARK: Arrange View
    func configureView() {
        
        backgroundColor = UIColor.lightGray
        layer.cornerRadius = 4
    }
    
    
}





