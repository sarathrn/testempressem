//
//  UIViewController.swift
//  TestEmpressem
//
//  Created by apple on 08/02/17.
//  Copyright © 2017 Sarath Raveendran. All rights reserved.
//

import Foundation
import UIKit
import TTGSnackbar

let activityIndicator = UIActivityIndicatorView()
extension UIViewController {
    
    // Show and Hide Loading indicator
    func showIndicator() {
        
        if activityIndicator.isDescendant(of: view.window!) {
            activityIndicator.startAnimating()
        }
        else {
            activityIndicator.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            activityIndicator.center = view.window!.center
            activityIndicator.activityIndicatorViewStyle = .whiteLarge
            activityIndicator.color = UIColor.red
            activityIndicator.hidesWhenStopped = true
            view.window?.addSubview(activityIndicator)
            activityIndicator.startAnimating()
        }
        // Disable user interaction
        UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    
    func hideIndicator() {
        if activityIndicator.isDescendant(of: view.window!) {
            activityIndicator.stopAnimating()
        }
        // Enable user interacton back
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    
    //set page title
    func setPageTitle(title: String) {
        
        self.navigationItem.title = title
        self.navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName:UIFont.boldSystemFont(ofSize: 12),NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    // Push view custom navigation item
    func configureNavigationBack() {
        
        let image = UIImage(named: "icon_back_arrow.png")!
        let navBarItem = UIBarButtonItem(image:image,  style: .plain, target: self, action: #selector(UIViewController.didTapNavigationBack(sender:)))
        navigationItem.leftBarButtonItem = navBarItem
        
    }
    
    // Page back Action
    func didTapNavigationBack(sender: AnyObject){
        _ = navigationController?.popViewController(animated: true)
    }
        
    
    // Show messages
    func showMessage(message: String) {
        let snackbar = TTGSnackbar.init(message: message, duration: TTGSnackbarDuration.short)
        snackbar.contentInset = UIEdgeInsets.init(top: 8, left: 8, bottom: 8, right: 8)
        snackbar.leftMargin = 8
        snackbar.rightMargin = 8
        snackbar.bottomMargin = 8
        snackbar.backgroundColor = UIColor(hexString: "1A8CEF")
        snackbar.animationDuration = 0.5
        snackbar.animationType = TTGSnackbarAnimationType.fadeInFadeOut
        snackbar.show()
    }
  
}
