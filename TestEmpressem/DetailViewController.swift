//
//  DetailViewController.swift
//  TestEmpressem
//
//  Created by apple on 09/02/17.
//  Copyright © 2017 Sarath Raveendran. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    // MARK: Connection Objects
    @IBOutlet weak var imageView: UIImageView!
    
    // Declarations
    // Set From Parent
    var image: UIImage?
    
    // MARK: Connection Objects
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Arrange View
    func configureView() {
        
        let rightBarButton = UIBarButtonItem(title: "Close", style: UIBarButtonItemStyle.plain, target: self, action: #selector(DetailViewController.dismissView(_:)))
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        // Set Image
        if let _image = image {
            imageView.image = _image
        }
        
    }
    
    // Dismiss View Controller
    func dismissView(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }

}
