//
//  PaddingTextField.swift
//  TestEmpressem
//
//  Created by apple on 08/02/17.
//  Copyright © 2017 Sarath Raveendran. All rights reserved.
//

import UIKit

class PaddingTextField: UITextField {

    // Declaration
    
    
    // MARK: View Cycle
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y, width: bounds.width, height: bounds.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y, width: bounds.width, height: bounds.height)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureView()
    }
    
    // MARK: Arrange View
    func configureView() {
        
        layer.cornerRadius = 2
        backgroundColor = UIColor.white
    }
    
    
}
