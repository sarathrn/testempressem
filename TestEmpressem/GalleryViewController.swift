//
//  GalleryViewController.swift
//  TestEmpressem
//
//  Created by apple on 09/02/17.
//  Copyright © 2017 Sarath Raveendran. All rights reserved.
//

import UIKit
import Photos


class GalleryViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {


    // Declarations
    var dataSource = Array<UIImage>()
    let limit = 10
    
    
    // MARK: Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
    }
   
    
    // MARK: UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return dataSource.count
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = UIScreen.main.bounds.width
        var size: CGSize!
        
        if width < 375 {
            let itemWidth = (width / 2) - 8
            let height = (itemWidth * 9 / 16 ) + 40
            size = CGSize(width: itemWidth, height: height )
        }
        else if width <= 414 {
            let itemWidth = (width / 3) - 8
            let height = (itemWidth * 9 / 16 ) + 40
            size = CGSize(width: itemWidth, height: height )
        }
        else if width > 414 {
            let itemWidth = (width / 4) - 8
            let height = (itemWidth * 9 / 16 ) + 40
            size = CGSize(width: itemWidth, height: height )
        }
        
        return size
    }

    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCollectionViewCell", for: indexPath) as! GalleryCollectionViewCell
        cell.image = dataSource[indexPath.item]
        return cell
        
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        let selectedImage = dataSource[indexPath.item]
        let detailsViewController = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        detailsViewController.image = selectedImage
        let naviagationVC = UINavigationController(rootViewController: detailsViewController)
        present(naviagationVC, animated: true, completion: nil)
    }
    
    
    // MARK: Arrange View
    func configureView() {
        
        // Collection Content Margins
        collectionView?.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        
    }
    
    
    // MARK: Events
    // Load data from gallery
    func loadData() {
        
        
        dataSource = Array<UIImage>()
        let imgManager = PHImageManager.default()
        
        // Synchronous = False -> return both the image and thumbnail
        // Synchronous = False -> will return only  thumbnail
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = true
        
        // Sort the images by creation date
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: true)]
        
        let fetchResult: PHFetchResult = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)
        if fetchResult.count > 0 {
            
            for i in 0..<limit  {
                
                let _asset = fetchResult.object(at: fetchResult.count - 1 - i)
                // Request Image
                imgManager.requestImage(for: _asset, targetSize: view.frame.size, contentMode: PHImageContentMode.aspectFill, options: requestOptions, resultHandler: { (image, _) in
                    
                    if let _image = image {
                        self.dataSource.append(_image)
                    }
                })
                
            }
            self.reload()
            
        }
    
    }
    
    // Reload data
    func reload() {
        DispatchQueue.main.async {
            self.collectionView?.reloadData()
        }
    }
    

}









