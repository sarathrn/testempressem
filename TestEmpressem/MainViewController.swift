//
//  MainViewController.swift
//  TestEmpressem
//
//  Created by apple on 09/02/17.
//  Copyright © 2017 Sarath Raveendran. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController, UITabBarControllerDelegate {

    // Declaration
    let color = Colors.sharedInstance
    
    
    // MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
   
        configureTabController()
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: ConfigureView
    func configureView() {
        
        delegate = self
        
    }
    
    
    // Set TabBar
    func configureTabController() {
        
        // Load View Controllers
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        // 1
        let cameraViewController = CameraViewController()
        // 2
        let galleryViewController = storyboard.instantiateViewController(withIdentifier: "GalleryViewController") as! GalleryViewController
        let galleryViewNavigation = UINavigationController(rootViewController: galleryViewController)
        // 3
        let profileViewController = storyboard.instantiateViewController(withIdentifier: "ProfileTableViewController") as! ProfileTableViewController
        let profileNavigation = UINavigationController(rootViewController: profileViewController)
        
        // Arrange Tab Items
        //1
        let tabOne = cameraViewController
        let tabOneBarItem = UITabBarItem(title: nil, image: UIImage(named: "camera")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named: "camera_highlighted")?.withRenderingMode(.alwaysOriginal))
        tabOne.tabBarItem = tabOneBarItem
        // 2
        let tabTwo = galleryViewNavigation
        let tabTwoBarItem = UITabBarItem(title: nil, image: UIImage(named: "gallery")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named: "gallery_highlighted")?.withRenderingMode(.alwaysOriginal))
        tabTwo.tabBarItem = tabTwoBarItem
        // 3
        let tabThree = profileNavigation
        let tabThreeBarItem = UITabBarItem(title: nil, image: UIImage(named: "profile")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named: "profile_highlighted")?.withRenderingMode(.alwaysOriginal))
        tabThree.tabBarItem = tabThreeBarItem
        
        // Make Tabbar
        self.viewControllers = [tabOne, tabTwo, tabThree]
        self.tabBar.itemPositioning = UITabBarItemPositioning.fill
        
        tabBar.barTintColor = color.themeColor
        tabBar.tintColor = UIColor.white
        // Default Home View Controller
        selectedIndex = 2
        
    }
    
    // MARK: Tabbar Data Source
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        if let cameraViewController = viewController as? CameraViewController {
            cameraViewController.awakeFromMenu()
        }
        return true
    }
    

}
