//
//  LoginViewController.swift
//  TestEmpressem
//
//  Created by apple on 08/02/17.
//  Copyright © 2017 Sarath Raveendran. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    //MARK: Connection Objects
    @IBOutlet weak var emailTextField: PaddingTextField!
    @IBOutlet weak var passwordTextField: PaddingTextField!
    @IBOutlet weak var loginButton: UIButton!
    
    // Declarations
    let colors = Colors.sharedInstance
    let warningMessages = WarnningMessages.sharedInstance
    let networkHanlder = NetworkHandler.sharedInstance
    let urls = APIUrls.sharedInstance
    let getPostKeys = GetPostKeys.sharedInstance
    let loginModal = LoginModal.sharedInstance
    
    
    
    //MARK: Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    //MARK: Arrange View
    func configureView() {
        
        view.backgroundColor = colors.loginPageBackground
        
    }
    

    // MARK: Events
    @IBAction func loginAction(_ sender: UIButton) {
       
        view.endEditing(true)
        
        // Validation
        guard let email = emailTextField.text else {
            showMessage(message: warningMessages.invalidEmail)
            return
        }
        guard let password = passwordTextField.text else {
            showMessage(message: warningMessages.invalidPassword)
            return
        }
        /*guard let token = RegisterModal.sharedInstance.user.token else {
            showMessage(message: warningMessages.invalidToken)
            return
        }*/
        
        
        
        // Preapre Network Action
        var params = Dictionary<String, Any>()
        params[getPostKeys.email] = email
        params[getPostKeys.password] = password
        params[getPostKeys.token] = APIToken.sharedInstance.token
            
        networkHanlder.post(url: urls.login, params: params, showLoader: true, vc: self) { (networkStatus, response) in
          
            // Network Status
            if !networkStatus {
                self.showMessage(message: self.warningMessages.noConnection)
                return
            }
            
            // Response
            if let _response = response {
                
                LoginParser().parse(response: _response as! Dictionary<String, Any>, modal: self.loginModal, completionHandler: {
                    
                    self.validateStatus()
                    
                })
            }
        }
        
        
    }
    
    func validateStatus() {
        
        // Check Status
        if let statusCode = loginModal.statusCode, let message = loginModal.message {
            
            // User Alert
            showMessage(message: message)
            switch statusCode {
            case APIStatus.success.rawValue :
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1), execute: {
                    self.presentHome()
                })
                
            default: break
            }
        }

    }
    
    // Presnt Home
    func presentHome() {
        let mainViewController = MainViewController()
        present(mainViewController, animated: true, completion: nil)
    }
    
}

