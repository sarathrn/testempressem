//
//  RegisterViewController.swift
//  TestEmpressem
//
//  Created by apple on 08/02/17.
//  Copyright © 2017 Sarath Raveendran. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    //MARK: Connection Objects
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    
    // Declaration
    let color = Colors.sharedInstance
    let warningMessages = WarnningMessages.sharedInstance
    let getPostKeys = GetPostKeys.sharedInstance
    let networkHanlder = NetworkHandler.sharedInstance
    let urls = APIUrls.sharedInstance
    var registerModal = RegisterModal.sharedInstance
    
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
    }

    
    //MARK: Arrange View
    func configureView() {
    
       view.backgroundColor = color.registerPageBackground
        
    }
    
    // MARK: Events
    @IBAction func signinAction(_ sender: UIButton) {
        view.endEditing(true)
        
        let loginVC = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        present(loginVC, animated: true, completion: nil)
    }
    
    @IBAction func registerAction(_ sender: UIButton) {
        
        view.endEditing(true)
        
        // Validation
        guard let name = nameTextField.text, name.isNotEmpty else {
            showMessage(message: warningMessages.invalidName)
            return
        }
        guard let userName = userNameTextField.text, userName.isNotEmpty else {
            showMessage(message: warningMessages.invalidUserName)
            return
        }
        
        guard let email = emailTextField.text, email.isNotEmpty, email.isValidEmail else {
            showMessage(message: warningMessages.invalidEmail)
            return
        }
        /*guard let password = passwordTextField.text, password.isNotEmpty, password.isValidPassword else {
            showMessage(message: warningMessages.invalidPassword)
            return
        }*/

        guard let password = passwordTextField.text, password.isNotEmpty else {
            showMessage(message: warningMessages.invalidPassword)
            return
        }
        
        
        // Preapre Network Action
        var params = Dictionary<String, Any>()
        params[getPostKeys.name] = name
        params[getPostKeys.username] = userName
        params[getPostKeys.email] = email
        params[getPostKeys.password] = password
        params[getPostKeys.token] = APIToken.sharedInstance.token
        
        // Network Action
        networkHanlder.post(url: urls.register, params: params, showLoader: true, vc: self) { (networkStatus, response) in
            
            // Network Status
            if !networkStatus {
                self.showMessage(message: self.warningMessages.noConnection)
                return
            }
            
            // Response
            if let _response = response {
           
                // Parse
                RegisterParser().parse(response: _response as! Dictionary<String, Any>, modal: self.registerModal, completionHandler: {
                    
                    self.validateStatus()
                    
                })
                
            }
            
        }
        
    }
    
    // Validate Response Status
    func validateStatus() {
       
        // Check Status
        if let statusCode = self.registerModal.statusCode, let message = self.registerModal.message {
            
            // User Alert
            showMessage(message: message)
            switch statusCode {
                case APIStatus.success.rawValue :
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1), execute: {
                        self.presentLogin()
                    })
                
                default: break
            }
        }
    }
    
    // Preset Register Page
    func presentLogin() {
        
        let loginViewController = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        present(loginViewController, animated: true, completion: nil)
    }
    

}
