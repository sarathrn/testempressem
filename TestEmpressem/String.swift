//
//  String.swift
//  TestEmpressem
//
//  Created by apple on 08/02/17.
//  Copyright © 2017 Sarath Raveendran. All rights reserved.
//

import Foundation

extension String {
    
    // Check not empty
    var isNotEmpty: Bool {
        let string  = self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return string.isEmpty ? false : true
    }
    
    // Email validation
    var isValidEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    // Validate password
    var isValidPassword: Bool {
        let passwordRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{6,15}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
    }
    
    // String to Boolean
    func toBool() -> Bool? {
        switch self.lowercased() {
        case "true", "yes", "1":
            return true
        case "false", "no", "0":
            return false
        default:
            return nil
        }
    }
}
